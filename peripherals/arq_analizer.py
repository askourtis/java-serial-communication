import matplotlib.pyplot as plt; plt.rcdefaults()
import sys
import operator
from typing import *
from math import pow

def main(args):
	if not isinstance(args, list):
		raise TypeError(f"Type expected {type([])}")
	args = args[1:]
	if len(args) != 3:
		raise ValueError("Three arguments are required: {FILENAME}, {CODE}, {TIME}")
	FILENAME = args[0]
	CODE = args[1]
	TIME = args[2]
	TITLE = f"[ARQ] [{TIME}] [{CODE}_request_code]"
	
	lines = []
	with open(FILENAME, 'r') as file:
		lines = file.readlines()
	lines = [line.strip() for line in lines]
	data = [tuple(line.split(',')) for line in lines]
	accepted = [int(i) for i in (map(lambda t: t[1], filter(lambda t: t[0] == 'A', data)))]
	
	PER = (len(data) - len(accepted))/len(data)
	BER = 1 - pow(1 - PER, 1/(16 * 8))
	print(f"PER = {PER}")
	print(f"BER = {BER}")
	
	plt.figure("ARQ")
	plt.bar(range(len(accepted)), accepted, align='center', alpha=0.8)
	plt.ylabel("Χρόνος απόκρισης (ms)")
	plt.xlabel("Πακέτο")
	plt.title(TITLE)
	plt.interactive(True)
	plt.show()
	
	plt.figure("ARQ FREQUENCY")
	freq = {}
	for point in accepted:
		if point in freq:
			freq[point] += 1
		else:
			freq[point] = 1
	
	plt.bar(list(freq.keys()), list(freq.values()), align='center', alpha=0.8)
	plt.ylabel("Συχνότητα εμφάνισης")
	plt.xlabel("Χρόνος απόκρισης (ms)")
	plt.title(TITLE)
	plt.interactive(True)
	plt.show()
	
	plt.figure("RESEND ARQ")
	times = [0]
	for point in data:
		times[-1] += 1
		if point[0] == 'A':
			times.append(0)
	del times[-1]
	plt.bar(range(len(times)), times, align='center', alpha=0.8)
	plt.ylabel("Αριθμός επανεκπομπών")
	plt.xlabel("Πακέτο")
	plt.title(TITLE)
	plt.interactive(True)
	plt.show()
	
	plt.figure("RESEND FREQUENCY")
	freq = {}
	for element in times:
		if element in freq:
			freq[element] += 1
		else:
			freq[element] = 1
	plt.bar(list(freq.keys()), list(freq.values()), align='center', alpha=0.8)
	plt.ylabel("Συχνότητα εμφάνισης")
	plt.xlabel("Αριθμός επανεκπομπών")
	plt.title(TITLE)
	plt.interactive(False)
	plt.show()
	
	
	

	
	
if __name__ == "__main__":
	main(sys.argv)