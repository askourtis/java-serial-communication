import matplotlib.pyplot as plt; plt.rcdefaults()
import sys
import operator
from typing import *


def main(args):
	if not isinstance(args, list):
		raise TypeError(f"Type expected {type([])}")
	args = args[1:]
	if len(args) != 3:
		raise ValueError("Three arguments are required: {FILENAME}, {CODE}, {TIME}")
	FILENAME = args[0]
	CODE = args[1]
	TIME = args[2]
	TITLE = f"[ECHO] [{TIME}] [{CODE}_request_code]"
	
	lines = []
	with open(FILENAME, 'r') as file:
		lines = file.readlines()
	lines = [line.strip() for line in lines]
	
	data = [int(line) for line in lines]
	


	plt.figure("ECHO")
	plt.bar(range(len(data)), data , align='center', alpha=0.8)
	plt.ylabel("Χρόνος απόκρισης (ms)")
	plt.xlabel("Πακέτο")
	plt.title(TITLE)
	plt.interactive(True)
	plt.show()
	
	plt.figure("ECHO FREQUENCY")
	freq = {}
	for point in data:
		if point in freq:
			freq[point] += 1
		else:
			freq[point] = 1

	plt.bar(list(freq.keys()), list(freq.values()), align='center', alpha=0.8)
	plt.ylabel("Συχνότητα εμφάνισης")
	plt.xlabel("Χρόνος απόκρισης (ms)")
	plt.title(TITLE)
	plt.interactive(False)
	plt.show()
	
	
if __name__ == "__main__":
	main(sys.argv)
	
