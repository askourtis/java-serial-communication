package skourtis;

/**
 * A wrap of all the needed codes for the server communication.
 * 
 * @author Antonis Skourtis
 *
 */
public enum Code {
	ECHO, IMG, CMG, GPS, ACK, NACK;

	/**
	 * Sets the codes' string value to the given arguments.
	 */
	public static void setCodes(String echo, String img, String cmg, String gps, String ack, String nack) {
		Code.ECHO.value = echo;
		Code.IMG.value = img;
		Code.CMG.value = cmg;
		Code.GPS.value = gps;
		Code.ACK.value = ack;
		Code.NACK.value = nack;
	}

	private String value;

	/**
	 * Returns the string value of the code.
	 * 
	 * @return the string value
	 */
	public String getStringValue() {
		return this.value;
	}
}
