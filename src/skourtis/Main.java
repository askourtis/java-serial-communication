package skourtis;

import javax.swing.*;
import javax.swing.border.*;
import skourtis.forms.frames.*;

/**
 * Entry class
 * 
 * @author Antonis Skourtis
 */
public class Main {

	// Constants
	public final static String SERVER_NAME = "ITHAKI";

	public final static String DATA_FOLDER = "data/";

	public final static byte[] WELCOME_MESSAGE_SUFFIX = "\r\n\n\n".getBytes();

	public final static byte[] MESSAGE_PREFIX = "PSTART".getBytes();
	public final static byte[] MESSAGE_SUFFIX = "PSTOP".getBytes();

	public final static byte[] IMAGE_PREFIX = new byte[] { (byte) 0xFF, (byte) 0xD8 };
	public final static byte[] IMAGE_SUFFIX = new byte[] { (byte) 0xFF, (byte) 0xD9 };

	public final static byte[] GPS_PREFIX = "START ITHAKI GPS TRACKING\r\n".getBytes();
	public final static byte[] GPS_SUFFIX = "STOP ITHAKI GPS TRACKING\r\n".getBytes();

	public final static byte[] EMPTY_BUFFER = new byte[0];

	/**
	 * Entry point
	 */
	public static void main(String[] args) {
		MainFrame mf = new MainFrame();
		SwingUtilities.invokeLater(() -> mf.setVisible(true));
	}

	/**
	 * Generates a new <i>pretty</i> titled border.
	 * 
	 * @param title - the title
	 * @return the border object
	 * @see {@link TitledBorder}, {@link CompoundBorder}
	 */
	public static Border generatePrettyBorder(String title) {
		return new TitledBorder(Main.generatePrettyBorder(), title, TitledBorder.LEADING, TitledBorder.TOP, null, null);
	}

	/**
	 * Generates a new <i>pretty</i> border.
	 * 
	 * @return the border object
	 * @see {@link CompoundBorder}
	 */
	public static Border generatePrettyBorder() {
		return new CompoundBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null),
				new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
	}

}
