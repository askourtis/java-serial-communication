package skourtis.forms.frames;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.*;
import skourtis.net.*;
import java.awt.event.*;

/**
 * The client dialog
 * 
 * @author Antonis Skourtis
 */
@SuppressWarnings("serial")
public class ClientDialog extends JDialog {

	private final Client client;
	private final Thread clientThread;
	private final CustomWorker worker;
	private final JButton btnStart;
	private final JButton btnStop;
	private final JTextArea txtOut;

	/**
	 * Create the dialog.
	 */
	public ClientDialog(Client client) {
		clientThread = new Thread(this.client = client, "Client Thread");
		worker = new CustomWorker();

		addWindowListener(new CustomWindowHandler());
		setModalityType(ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		setTitle("Client Dialog");
		setBounds(100, 100, 900, 600);
		getContentPane().setLayout(null);

		btnStop = new JButton("Stop");
		btnStop.addActionListener(this::onBtnStopClick);
		btnStop.setEnabled(false);
		btnStop.setBounds(795, 537, 89, 23);
		getContentPane().add(btnStop);

		btnStart = new JButton("Start");
		btnStart.addActionListener(this::onBtnStartClick);
		btnStart.setBounds(10, 537, 89, 23);
		getContentPane().add(btnStart);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 874, 516);
		getContentPane().add(scrollPane);

		txtOut = new JTextArea();
		txtOut.setEditable(false);
		scrollPane.setViewportView(txtOut);
	}

	/**
	 * Handles the click event of the button <b>Start</b>
	 */
	private void onBtnStartClick(ActionEvent e) {
		this.worker.execute();
		this.clientThread.start();
		this.btnStop.setEnabled(true);
		this.btnStart.setEnabled(false);
	}

	/**
	 * Handles the click event of the button <b>Stop</b>
	 */
	private void onBtnStopClick(ActionEvent e) {
		this.client.setCancel(true);
		this.btnStop.setEnabled(false);
	}

	/**
	 * A custom window handler to stop the threads when the window is closed.
	 * 
	 * @author Antonis Skourtis
	 * @see {@link WindowAdapter}
	 */
	private class CustomWindowHandler extends WindowAdapter {
		@Override
		public void windowClosed(WindowEvent e) {
			ClientDialog.this.client.setCancel(true);
			ClientDialog.this.worker.cancel(true);
			if (ClientDialog.this.clientThread.isAlive())
				try {
					ClientDialog.this.clientThread.join();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			ClientDialog.this.client.close();
		}
	}

	/**
	 * A custom update worker.
	 * 
	 * @author Antonis Skourtis
	 * @see {@link SwingWorker}
	 */
	private class CustomWorker extends SwingWorker<Void, Byte> {

		private final InputStream is;

		public CustomWorker() {
			InputStream is;
			try {
				is = ClientDialog.this.client.getInputStream();
			} catch (IOException e) {
				is = null;
				JOptionPane.showMessageDialog(ClientDialog.this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
			this.is = is;
		}

		@Override
		protected Void doInBackground() throws Exception {
			if (this.is != null) {
				while (!this.isCancelled() && ClientDialog.this.clientThread.isAlive()) {
					int in;
					while (!this.isCancelled() && (in = this.is.read()) != -1)
						this.publish((byte) in);
				}
			}

			return null;
		}

		@Override
		protected void done() {
			ClientDialog.this.btnStop.setEnabled(false);
		}

		@Override
		protected void process(List<Byte> chunks) {
			ClientDialog.this.txtOut.append(chunks.stream().map((b) -> new Character((char) b.byteValue()))
					.map(String::valueOf).collect(Collectors.joining()));
		}

	}

}
