package skourtis.forms.frames;

import javax.swing.*;
import javax.swing.border.*;

import skourtis.Code;
import skourtis.forms.panels.*;
import skourtis.net.*;
import skourtis.utils.PairView;

import java.awt.event.*;
import java.util.*;

/**
 * The main frame of the program.
 * 
 * @author Antonis Skourtis
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private final SettingsPanel panelSettings;
	private final StatisticsPanel panelEcho;
	private final StatisticsPanel panelARQ;
	private final ImagePanel panelImage;
	private final GPSPanel panelGPS;

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setResizable(false);
		setTitle("Client Configuration");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 697, 374);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 671, 270);
		contentPane.add(tabbedPane);

		panelSettings = new SettingsPanel();
		tabbedPane.addTab("Settings", null, panelSettings, null);
		panelSettings.setLayout(null);

		panelEcho = new StatisticsPanel();
		tabbedPane.addTab("Echo", null, panelEcho, null);
		panelEcho.setLayout(null);

		panelARQ = new StatisticsPanel();
		tabbedPane.addTab("ARQ", null, panelARQ, null);
		panelARQ.setLayout(null);

		panelImage = new ImagePanel();
		tabbedPane.addTab("Image", null, panelImage, null);
		panelImage.setLayout(null);

		panelGPS = new GPSPanel();
		tabbedPane.addTab("GPS", null, panelGPS, null);

		JButton btnRun = new JButton("Run");
		btnRun.addActionListener(this::onBtnRunClick);
		btnRun.setBounds(592, 308, 89, 23);
		contentPane.add(btnRun);
	}

	/**
	 * Handles the click event of the button <b>Run</b>
	 */
	private void onBtnRunClick(ActionEvent e) {
		List<Command> commands = new LinkedList<Command>();
		int speed;
		int timeout;
		int echoValue;
		int arqValue;

		this.panelSettings.setCodes();
		try {
			speed = this.panelSettings.getSpeed();
			timeout = this.panelSettings.getTimeout();
			echoValue = this.panelEcho.getValue();
			arqValue = this.panelARQ.getValue();
		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(this, "Input was not a number.\n" + ex.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		if (this.panelEcho.isAmount()) {
			for (int i = 0; i < echoValue; ++i)
				commands.add(new Command(Code.ECHO));
		} else {
			commands.add(new TimedCommand(Code.ECHO, echoValue));
		}

		if (this.panelARQ.isAmount()) {
			for (int i = 0; i < arqValue; ++i)
				commands.add(new Command(Code.ACK));
		} else {
			commands.add(new TimedCommand(Code.ACK, arqValue));
		}

		for (PairView<Boolean, String[]> pair : this.panelImage.getParams())
			commands.add(new Command(pair.getFirst() ? Code.IMG : Code.CMG, pair.getSecond()));
		for (String[] params : this.panelGPS.getParams()) {
			if (params.length == 3 && params[0].startsWith("T=R=") && params[1].startsWith("N=")
					&& params[2].startsWith("T=")) {

				try {
					String TR = params[0].substring(4);
					String N = params[1].substring(2);
					String T = params[2].substring(2);
					int route = Integer.parseUnsignedInt(String.valueOf(TR.charAt(0)));
					int trace = Integer.parseUnsignedInt(TR.substring(1));
					int number = Integer.parseUnsignedInt(N);
					int timediff = Integer.parseUnsignedInt(T);
					commands.add(new SpecialGPSCommand(route, trace, number, timediff));
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(this, String.format("Expected number: %s", ex.getMessage()), "Error",
							JOptionPane.ERROR_MESSAGE);
				}

				continue;
			}
			commands.add(new Command(Code.GPS, params));
		}

		Client client = new Client(commands, speed, timeout);
		ClientDialog cd = new ClientDialog(client);
		cd.setVisible(true);

	}
}
