package skourtis.forms.panels;

import javax.swing.*;
import java.util.*;
import java.util.stream.*;
import java.awt.*;
import java.awt.event.*;

/**
 * A panel that handles the gps parameters of the program.
 * 
 * @author Antonis Skourtis
 */
@SuppressWarnings("serial")
public class GPSPanel extends JPanel {

	private final JTextField txtParam;
	private final DefaultListModel<String[]> model;
	private final JList<String[]> list;

	/**
	 * Default constructor.
	 */
	public GPSPanel() {
		setLayout(null);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(this::onBtnAddClick);
		btnAdd.setBounds(567, 44, 89, 23);
		add(btnAdd);

		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(this::onBtnRemoveClick);
		btnRemove.setBounds(567, 107, 89, 23);
		add(btnRemove);

		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(this::onBtnClearClick);
		btnClear.setBounds(567, 135, 89, 23);
		add(btnClear);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 44, 546, 185);
		add(scrollPane);

		model = new DefaultListModel<>();

		list = new JList<>();
		list.setModel(model);
		list.setCellRenderer(new CustomCellRenderer());
		scrollPane.setViewportView(list);

		txtParam = new JTextField();
		txtParam.addKeyListener(new CustomKeyAdapter());
		txtParam.setBounds(10, 11, 646, 20);
		add(txtParam);
		txtParam.setColumns(10);
	}

	/**
	 * Adds the parameter to the list.
	 */
	private void acceptParam() {
		String text = this.txtParam.getText().trim();
		String[] args = (text.isEmpty()) ? (new String[0]) : (text.split("\\s+"));
		this.model.addElement(args);
		this.txtParam.setText("");
	}

	/**
	 * Handles the click event of the button <b>Add</b>.
	 */
	private void onBtnAddClick(ActionEvent e) {
		this.acceptParam();
	}

	/**
	 * Handles the click event of the button <b>Remove</b>.
	 */
	private void onBtnRemoveClick(ActionEvent e) {
		int[] indices = this.list.getSelectedIndices();
		for (int i = 0; i < indices.length; ++i) {
			this.model.remove(indices[i] - i);
		}
	}

	/**
	 * Handles the click event of the button <b>Clear</b>.
	 */
	private void onBtnClearClick(ActionEvent e) {
		this.model.clear();
	}

	/**
	 * Returns a list of the parameters.
	 * 
	 * @return the parameters
	 */
	public java.util.List<String[]> getParams() {
		LinkedList<String[]> ret = new LinkedList<>();
		for (int i = 0; i < this.model.getSize(); ++i)
			ret.add(this.model.get(i));
		return ret;
	}

	/**
	 * A custom key adapter for the parameter text box.
	 * 
	 * @author Antonis Skourtis
	 * @see {@link KeyAdapter}
	 */
	private class CustomKeyAdapter extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER)
				GPSPanel.this.acceptParam();
		}

	}

	/**
	 * A custom cell renderer for the list.
	 * 
	 * @author Antonis Skourtis
	 * @see {@link DefaultListCellRenderer}
	 */
	private class CustomCellRenderer extends DefaultListCellRenderer {
		public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
				boolean cellHasFocus) {
			super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			if (value instanceof String[]) {
				String[] cast_value = (String[]) value;
				this.setText(Arrays.asList(cast_value).stream().map(str -> String.format("[%s]", str))
						.collect(Collectors.joining("", "[", "]")));
			}

			return this;
		}
	}
}
