package skourtis.forms.panels;

import javax.swing.*;

import skourtis.*;
import skourtis.forms.utils.UIntegerKeyAdapter;

/**
 * A panel that handles the settings parameters of the program.
 * 
 * @author Antonis Skourtis
 */
@SuppressWarnings("serial")
public class SettingsPanel extends JPanel {

	private final JTextField txtEcho;
	private final JTextField txtImage;
	private final JTextField txtCorImage;
	private final JTextField txtGps;
	private final JTextField txtAck;
	private final JTextField txtNack;
	private final JTextField txtSpeed;
	private final JTextField txtTimeout;

	/**
	 * Default constructor.
	 */
	public SettingsPanel() {
		setLayout(null);

		JPanel panelCode = new JPanel();
		panelCode.setLayout(null);
		panelCode.setBorder(Main.generatePrettyBorder("Code Settings"));
		panelCode.setBounds(90, 27, 238, 183);
		add(panelCode);

		JLabel label = new JLabel("ECHO:");
		label.setBounds(20, 28, 93, 14);
		panelCode.add(label);

		JLabel label_1 = new JLabel("IMAGE:");
		label_1.setBounds(20, 53, 93, 14);
		panelCode.add(label_1);

		JLabel label_2 = new JLabel("Cor. IMAGE:");
		label_2.setBounds(20, 78, 93, 14);
		panelCode.add(label_2);

		JLabel label_3 = new JLabel("GPS:");
		label_3.setBounds(20, 103, 93, 14);
		panelCode.add(label_3);

		JLabel label_4 = new JLabel("ACK:");
		label_4.setBounds(20, 128, 93, 14);
		panelCode.add(label_4);

		JLabel label_5 = new JLabel("NACK:");
		label_5.setBounds(20, 153, 93, 14);
		panelCode.add(label_5);

		txtEcho = new JTextField();
		txtEcho.setHorizontalAlignment(SwingConstants.CENTER);
		txtEcho.setColumns(10);
		txtEcho.setBounds(123, 25, 99, 20);
		panelCode.add(txtEcho);

		txtImage = new JTextField();
		txtImage.setHorizontalAlignment(SwingConstants.CENTER);
		txtImage.setColumns(10);
		txtImage.setBounds(123, 50, 99, 20);
		panelCode.add(txtImage);

		txtCorImage = new JTextField();
		txtCorImage.setHorizontalAlignment(SwingConstants.CENTER);
		txtCorImage.setColumns(10);
		txtCorImage.setBounds(123, 75, 99, 20);
		panelCode.add(txtCorImage);

		txtGps = new JTextField();
		txtGps.setHorizontalAlignment(SwingConstants.CENTER);
		txtGps.setColumns(10);
		txtGps.setBounds(123, 100, 99, 20);
		panelCode.add(txtGps);

		txtAck = new JTextField();
		txtAck.setHorizontalAlignment(SwingConstants.CENTER);
		txtAck.setColumns(10);
		txtAck.setBounds(123, 125, 99, 20);
		panelCode.add(txtAck);

		txtNack = new JTextField();
		txtNack.setHorizontalAlignment(SwingConstants.CENTER);
		txtNack.setColumns(10);
		txtNack.setBounds(123, 150, 99, 20);
		panelCode.add(txtNack);

		JPanel panelSettings = new JPanel();
		panelSettings.setLayout(null);
		panelSettings.setBorder(Main.generatePrettyBorder("Client Settings"));
		panelSettings.setBounds(338, 79, 252, 80);
		add(panelSettings);

		JLabel label_6 = new JLabel("Speed:");
		label_6.setBounds(22, 23, 75, 14);
		panelSettings.add(label_6);

		JLabel label_7 = new JLabel("Timeout:");
		label_7.setBounds(22, 48, 75, 14);
		panelSettings.add(label_7);

		txtSpeed = new JTextField();
		txtSpeed.addKeyListener(new UIntegerKeyAdapter());
		txtSpeed.setText("80000");
		txtSpeed.setHorizontalAlignment(SwingConstants.CENTER);
		txtSpeed.setColumns(10);
		txtSpeed.setBounds(107, 20, 135, 20);
		panelSettings.add(txtSpeed);

		txtTimeout = new JTextField();
		txtTimeout.addKeyListener(new UIntegerKeyAdapter());
		txtTimeout.setText("1000");
		txtTimeout.setHorizontalAlignment(SwingConstants.CENTER);
		txtTimeout.setColumns(10);
		txtTimeout.setBounds(107, 45, 135, 20);
		panelSettings.add(txtTimeout);
	}

	/**
	 * Sets the codes to the given values.
	 */
	public void setCodes() {
		Code.setCodes(this.txtEcho.getText().trim(), this.txtImage.getText().trim(), this.txtCorImage.getText().trim(),
				this.txtGps.getText().trim(), this.txtAck.getText().trim(), this.txtNack.getText().trim());
	}

	/**
	 * Converts the text to an integer and returns the value.
	 * 
	 * @return the given speed.
	 * @throws NumberFormatException - if the input was not a number.
	 */
	public int getSpeed() throws NumberFormatException {
		return Integer.parseUnsignedInt(this.txtSpeed.getText());
	}

	/**
	 * Converts the text to an integer and returns the value.
	 * 
	 * @return the given timeout.
	 * @throws NumberFormatException - if the input was not a number.
	 */
	public int getTimeout() throws NumberFormatException {
		return Integer.parseUnsignedInt(this.txtTimeout.getText());
	}

}
