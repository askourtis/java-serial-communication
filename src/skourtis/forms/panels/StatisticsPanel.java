package skourtis.forms.panels;

import javax.swing.*;
import skourtis.Main;
import skourtis.forms.utils.UIntegerKeyAdapter;

/**
 * A panel that handles the statistics parameters of the program.
 * 
 * @author Antonis Skourtis
 */
@SuppressWarnings("serial")
public class StatisticsPanel extends JPanel {

	private final JTextField txtValue;
	private final JRadioButton rdbtnAmount;
	private final JRadioButton rdbtnTime;

	/**
	 * Default constructor.
	 */
	public StatisticsPanel() {
		setLayout(null);

		JPanel panelValue = new JPanel();
		panelValue.setLayout(null);
		panelValue.setBorder(Main.generatePrettyBorder("Value"));
		panelValue.setBounds(228, 146, 203, 58);
		add(panelValue);

		JLabel label = new JLabel("Value:");
		label.setBounds(24, 26, 46, 14);
		panelValue.add(label);

		txtValue = new JTextField();
		txtValue.addKeyListener(new UIntegerKeyAdapter());
		txtValue.setText("0");
		txtValue.setHorizontalAlignment(SwingConstants.CENTER);
		txtValue.setColumns(10);
		txtValue.setBounds(80, 23, 86, 20);
		panelValue.add(txtValue);

		JPanel panelTime = new JPanel();
		panelTime.setLayout(null);
		panelTime.setBorder(Main.generatePrettyBorder("Type"));
		panelTime.setBounds(263, 42, 133, 93);
		add(panelTime);

		rdbtnAmount = new JRadioButton("Amount");
		rdbtnAmount.setSelected(true);
		rdbtnAmount.setBounds(27, 25, 92, 23);
		panelTime.add(rdbtnAmount);

		rdbtnTime = new JRadioButton("Time (ms)");
		rdbtnTime.setBounds(27, 51, 92, 23);
		panelTime.add(rdbtnTime);

		ButtonGroup bg = new ButtonGroup();
		bg.add(rdbtnAmount);
		bg.add(rdbtnTime);
	}

	/**
	 * Returns the value type.
	 * 
	 * @return true if the value type is amount, returns false if the type is time
	 */
	public boolean isAmount() {
		return this.rdbtnAmount.isSelected();
	}

	/**
	 * Converts the given string from the text box to unsigned integer and returns
	 * the result.
	 * 
	 * @return the value
	 * @throws NumberFormatException - if the input was not a number.
	 */
	public int getValue() throws NumberFormatException {
		return Integer.parseUnsignedInt(this.txtValue.getText());
	}
}
