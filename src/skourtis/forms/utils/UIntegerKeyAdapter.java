package skourtis.forms.utils;

import java.awt.*;
import java.awt.event.*;
import javax.swing.text.*;

/**
 * A {@linkplain KeyAdapter} for {@linkplain JTextComponent JTextComponents}
 * that only accepts unsigned {@linkplain Integer Integers}. No characters other
 * than numbers are allowed.
 * 
 * @author Antonis Skourtis
 */
public class UIntegerKeyAdapter extends KeyAdapter {

	@Override
	public void keyTyped(KeyEvent e) {
		Component comp = e.getComponent();
		if (comp instanceof JTextComponent) {
			JTextComponent txtComp = (JTextComponent) comp;
			char ch = e.getKeyChar();
			if ((ch == KeyEvent.VK_BACK_SPACE || ch == KeyEvent.VK_DELETE) && txtComp.getText().isEmpty()) {
				e.consume();
				txtComp.setText("0");
			} else if (!(ch >= '0' && ch <= '9')) {
				e.consume();
			} else if (txtComp.getText().equals("0")) {
				txtComp.setText("");
			}
		}
	}

}
