package skourtis.net;

import java.io.*;
import java.util.*;
import java.util.stream.*;

import ithakimodem.*;
import skourtis.*;
import skourtis.utils.*;

/**
 * A process that can be cancelled, responsible for the correct transmission of
 * the commands to the server and also the process of the returned responses.
 * 
 * @author Antonis Skourtis
 * @see {@link Cancelable}
 */
public class Client extends Cancelable implements Closeable {

	private final Modem modem = new Modem();

	private final LinkedList<Command> commandStack = new LinkedList<>();
	private final Stopwatch timer = new Stopwatch();
	private Command command = null;
	private Response response = null;

	private final PipedOutputStream raw_out = new PipedOutputStream();
	private final PrintWriter out = new PrintWriter(this.raw_out, true);

	private final LinkedList<Long> echoStatistics = new LinkedList<>();
	private final LinkedList<PairView<Character, Long>> arqStatistics = new LinkedList<>();

	/**
	 * Constructs the object with the given values.
	 * 
	 * @param commands - A stack represented by a list with the commands to be sent
	 *                 to the server.
	 * @param speed    - the speed of the modem in (bps)
	 * @param timeout  - the timeout of the modem in (ms)
	 */
	public Client(List<? extends Command> commands, int speed, int timeout) {
		this.modem.setSpeed(speed);
		this.modem.setTimeout(timeout);
		this.commandStack.addAll(commands);
	}

	/**
	 * Main loop SEND-READ-PROCESS-REPEAT
	 */
	@Override
	public void run() {
		if (this.modem.open(Main.SERVER_NAME)) {
			this.response = this.readWelcomeMessage();

			if (this.response != null) {
				byte[] buffer = this.response.getBuffer();
				this.out.println(new String(buffer, 0, buffer.length - Main.WELCOME_MESSAGE_SUFFIX.length));
			} else {
				this.out.println("Did not receive welcome message.");
				this.setCancel(true);
			}

			while (!this.isCancelled() && !this.commandStack.isEmpty()) {
				this.command = this.commandStack.pop();

				this.processCommand();

				if (!this.sendCurrentCommand()) {
					this.out.println("Could not send a command to the server.");
					break;
				}

				if ((response = this.readResponse()) == null) {
					this.out.println("Could not get appropriate response.");
					continue;
				}

				if (!this.processResponse()) {
					this.out.println("Could not process the response.");
				}

			}
		} else {
			this.out.println("Could not connect.");
			this.setCancel(true);
		}

		this.out.println("---Session was concluded---");

		if (!this.isCancelled())
			this.whenDone();
		this.close();

	}

	/**
	 * Processes the current command. Only cases such as {@linkplain TimedCommand}
	 * and {@linkplain SpecialGPSCommand} will be processed.
	 */
	private void processCommand() {
		if (this.command instanceof TimedCommand) {
			if (!this.timer.isRunning())
				this.timer.start();
			if (this.timer.getDuration() < ((TimedCommand) this.command).getTime())
				this.commandStack.push(this.command);
			else
				this.timer.stop();
		} else if (this.command instanceof SpecialGPSCommand) {
			SpecialGPSCommand current = ((SpecialGPSCommand) this.command);

			if (current.isActive())
				current.processData(this.response.getBuffer());

			Command next = current.getNextCommand();

			if (next != null) {
				this.commandStack.push(current);
				this.command = next;
			}
		}
	}

	/**
	 * Sends the current command to the server.
	 * 
	 * @return true if the command was sent, false otherwise.
	 */
	private boolean sendCurrentCommand() {
		this.out.printf("\n[%s REQUEST]\n", this.command.getCode().toString());
		return this.modem.write(this.command.getBytes());
	}

	/**
	 * Reads the appropriate response from the server, given the current command.
	 * 
	 * @return the response of the server or null if no response.
	 */
	private Response readResponse() {
		this.out.println("Fetching Data...");
		switch (this.command.getCode()) {
		case NACK:
		case ACK:
		case ECHO:
			return this.readNextMessage();
		case GPS:
			return this.readNextGPS();
		case CMG:
		case IMG:
			return this.readNextImage();
		default:
			return null;
		}

	}

	/**
	 * Reads the welcome message of the server.
	 * 
	 * @return the welcome message as a response or null if no appropriate response.
	 * @see {@link Response}
	 */
	private Response readWelcomeMessage() {
		return this.readUntil(Main.WELCOME_MESSAGE_SUFFIX);
	}

	/**
	 * Reads the next message from the server. A message is defined to be a response
	 * that starts with "PSTART" and stops at "PSTOP".
	 * 
	 * @return the next message as a response or null if no appropriate response.
	 * @see {@link Response}
	 */
	private Response readNextMessage() {
		return this.readBetween(Main.MESSAGE_PREFIX, Main.MESSAGE_SUFFIX);
	}

	/**
	 * Reads the next gps response from the server.
	 * 
	 * @return the next gps response or null if no appropriate response.
	 * @see {@link Response}
	 */
	private Response readNextGPS() {
		String[] args = this.command.getArgs();
		if (args.length != 0 && args[0].startsWith("T"))
			return this.readNextImage();

		return this.readBetween(Main.GPS_PREFIX, Main.GPS_SUFFIX);
	}

	/**
	 * Reads the next image from the server.
	 * 
	 * @return the next image as a response or null if no appropriate response.
	 * @see {@link Response}
	 */
	private Response readNextImage() {
		return this.readBetween(Main.IMAGE_PREFIX, Main.IMAGE_SUFFIX, 100 * 1024 /* 100kB */);
	}

	/**
	 * Processes the given response.
	 * 
	 * @param response - the given response.
	 * @return true if the response was processed successfully, false otherwise.
	 * @see {@link Response}
	 */
	private boolean processResponse() {
		Code code = this.command.getCode();
		this.out.println("Processing response...");
		this.out.printf("[%s RESPONSE]\t", code.toString());

		switch (code) {
		case ACK:
		case NACK:
			return this.onARQ();
		case ECHO:
			return this.onEcho();
		case GPS:
			return this.onGPS();
		case CMG:
		case IMG:
			return this.onImage();
		default:
			return false;
		}
	}

	/**
	 * Processes the given response as an image.
	 * 
	 * @param response
	 * @return true if the response was processed successfully, false otherwise.
	 * @see {@link Response}
	 */
	private boolean onImage() {
		File out = new File(Main.DATA_FOLDER, String.format("UNNAMED%d.jpg", System.currentTimeMillis()));
		for (int i = 0; i < Integer.MAX_VALUE; ++i) {
			File tmp = new File(Main.DATA_FOLDER, String.format("Image%d.jpg", i));
			if (!tmp.exists()) {
				out = tmp;
				break;
			}
		}

		out.getParentFile().mkdirs();

		try {
			FileOutputStream fos = new FileOutputStream(out);
			fos.write(this.response.getBuffer());
			fos.close();
			this.out.printf("Image saved at: %s\n", out.getPath());
			return true;
		} catch (Exception e) {
			this.out.println();
			e.printStackTrace(this.out);
			this.out.flush();
			return false;
		}
	}

	/**
	 * Processes the given response as a gps.
	 * 
	 * @param response
	 * @return true if the response was processed successfully, false otherwise.
	 * @see {@link Response}
	 */
	private boolean onGPS() {
		String[] args = this.command.getArgs();
		if (args.length != 0 && args[0].startsWith("T")) {
			return this.onImage();
		}

		try {
			byte[] buffer = this.response.getBuffer();
			String str = new String(buffer, Main.GPS_PREFIX.length,
					buffer.length - Main.GPS_PREFIX.length - Main.GPS_PREFIX.length);
			String[] split = str.split("\r\n");
			for (String line : split) {
				String[] line_split = line.split(",");
				if (line_split[0].equals("$GPGGA")) {
					String latitude = line_split[2];
					String longitude = line_split[4];

					int latDeg = Integer.parseUnsignedInt(latitude.substring(0, 2));
					float latMinf = Float.parseFloat(latitude.substring(2));
					int latMin = (int) latMinf;
					int latSec = (int) ((latMinf - latMin) * 60);

					int longDeg = Integer.parseUnsignedInt(longitude.substring(0, 3));
					float longMinf = Float.parseFloat(longitude.substring(3));
					int longMin = (int) longMinf;
					int longSec = (int) ((longMinf - longMin) * 60);

					String T = String.format("T=%d%d%d%d%d%d", longDeg, longMin, longSec, latDeg, latMin, latSec);
					this.out.printf("\n%s\t%s", line,  T);
				} else {
					this.out.printf("\n%s\t(Line could not be translated)", line);
				}
			}
			this.out.println();

		} catch (Exception e) {
			this.out.println();
			e.printStackTrace(this.out);
			this.out.flush();
			return false;
		}

		return true;
	}

	/**
	 * Processes the given response as an ARQ.
	 * 
	 * @param response
	 * @return true if the response was processed successfully, false otherwise.
	 * @see {@link Response}
	 */
	private boolean onARQ() {
		try {
			byte[] buffer = this.response.getBuffer();
			buffer = Arrays.copyOfRange(buffer, Main.MESSAGE_PREFIX.length + 1,
					buffer.length - Main.MESSAGE_SUFFIX.length - 1);
			long latency = this.response.getLatency();

			String str = new String(buffer);
			String[] split = str.split(" ");
			byte[] packet = split[3].substring(1, split[3].length() - 1).getBytes();
			byte key = Byte.parseByte(split[4]);

			for (byte b : packet)
				key ^= b;

			if (key == 0) {
				for (int i = this.arqStatistics.size() - 1; i >= 0; --i) {
					PairView<Character, Long> element = this.arqStatistics.get(i);
					if (element.getFirst().equals('A'))
						break;
					latency += element.getSecond();
				}
				this.out.printf("(%s)\tOK\t(%dms)\n", str, latency);
				this.arqStatistics.add(new PairView<>('A', latency));
			} else {
				this.out.printf("(%s)\tNOT OK\t(%dms)\n", str, latency);
				this.commandStack.push(new Command(Code.NACK));
				this.arqStatistics.add(new PairView<>('N', latency));
			}

		} catch (Exception e) {
			this.out.println();
			e.printStackTrace(this.out);
			this.out.flush();
			return false;
		}

		return true;
	}

	/**
	 * Processes the given response as an echo.
	 * 
	 * @param response
	 * @return true if the response was processed successfully, false otherwise.
	 * @see {@link Response}
	 */
	private boolean onEcho() {
		try {
			byte[] buffer = this.response.getBuffer();
			buffer = Arrays.copyOfRange(buffer, Main.MESSAGE_PREFIX.length + 1,
					buffer.length - Main.MESSAGE_SUFFIX.length - 1);
			long latency = this.response.getLatency();
			this.out.printf("%s\t(%dms)\n", new String(buffer), latency);
			this.echoStatistics.add(latency);
			return true;
		} catch (Exception e) {
			this.out.println();
			e.printStackTrace(this.out);
			this.out.flush();
			return false;
		}

	}

	/**
	 * Gets the input stream of the client.
	 * 
	 * @return the InputStream of the client.
	 * @throws IOException - if an I/O error occurs.
	 * @see {@link PipedInputStream},
	 *      {@link PipedInputStream#PipedInputStream(PipedOutputStream)}
	 */
	public PipedInputStream getInputStream() throws IOException {
		return new PipedInputStream(this.raw_out);
	}

	/**
	 * Reads a response between a prefix and a suffix.
	 * 
	 * @param prefix   - the prefix
	 * @param suffix   - the suffix
	 * @param capacity - the initial capacity of the byte buffer/array
	 * @return the response (including the prefix and suffix) or null if no
	 *         appropriate response
	 * @see {@link Response}
	 */
	private Response readBetween(byte[] prefix, byte[] suffix, int capacity) {
		Stopwatch stopwatch = new Stopwatch();
		DynamicByteBuffer buffer = new DynamicByteBuffer(capacity);
		stopwatch.start();
		int in;
		while (!this.isCancelled() && (in = this.modem.read()) != -1) {
			buffer.append((byte) in);
			if (buffer.startsWith(prefix) && buffer.endsWith(suffix)) {
				stopwatch.stop();
				return new Response(buffer.toArray(), stopwatch.getDuration());
			}
		}
		return null;
	}

	/**
	 * Reads a response between a prefix and a suffix. Also sets the initial
	 * capacity to 512 bytes.
	 * 
	 * @param prefix - the prefix
	 * @param suffix - the suffix
	 * @return the response (including the prefix and suffix) or null if no
	 *         appropriate response
	 * @see {@link Client#readBetween(byte[], byte[], int)}, {@link Response}
	 */
	private Response readBetween(byte[] prefix, byte[] suffix) {
		return this.readBetween(prefix, suffix, 512);
	}

	/**
	 * Reads a response until a suffix.
	 * 
	 * @param suffix   - the suffix
	 * @param capacity - the initial capacity of the byte buffer/array
	 * @return the response (including the suffix) or null if no appropriate
	 *         response
	 * @see {@link Response}
	 */
	private Response readUntil(byte[] suffix, int capacity) {
		return this.readBetween(Main.EMPTY_BUFFER, suffix, capacity);
	}

	/**
	 * Reads a response until a suffix. Also sets the initial capacity to 512 bytes.
	 * 
	 * @param suffix - the suffix
	 * @return the response (including the suffix) or null if no appropriate
	 *         response
	 * @see {@link Client#readUntil(byte[], int)}, {@link Response}
	 */
	private Response readUntil(byte[] suffix) {
		return this.readUntil(suffix, 512);
	}

	/**
	 * Saves the statistic data Echo and ARQ to the disk.
	 */
	private void whenDone() {
		File dataFolder = new File(Main.DATA_FOLDER);
		File echoOut = new File(dataFolder, String.format("UNNAMED_ECHO%d.txt", System.currentTimeMillis()));
		File arqOut = new File(dataFolder, String.format("UNNAMED_ARQ%d.txt", System.currentTimeMillis()));

		if (!this.echoStatistics.isEmpty()) {
			for (int i = 0; i < Integer.MAX_VALUE; ++i) {
				File tmp = new File(Main.DATA_FOLDER, String.format("ECHO_STATS%d.txt", i));
				if (!tmp.exists()) {
					echoOut = tmp;
					break;
				}
			}

			dataFolder.mkdirs();

			try {
				FileOutputStream fos = new FileOutputStream(echoOut);
				fos.write(this.echoStatistics.stream().map(String::valueOf)
						.collect(Collectors.joining(System.lineSeparator())).getBytes());
				fos.close();
				this.out.printf("Saved ECHO statistics on file: %s\n", echoOut.getPath());
			} catch (Exception e) {
				e.printStackTrace(this.out);
				this.out.flush();
			}

		}

		if (!this.arqStatistics.isEmpty()) {
			for (int i = 0; i < Integer.MAX_VALUE; ++i) {
				File tmp = new File(Main.DATA_FOLDER, String.format("ARQ_STATS%d.txt", i));
				if (!tmp.exists()) {
					arqOut = tmp;
					break;
				}
			}

			dataFolder.mkdirs();

			try {
				FileOutputStream fos = new FileOutputStream(arqOut);
				fos.write(this.arqStatistics.stream().map(t -> String.format("%s,%s", t.getFirst(), t.getSecond()))
						.collect(Collectors.joining(System.lineSeparator())).getBytes());
				fos.close();
				this.out.printf("Saved ARQ statistics on file: %s\n", arqOut.getPath());
			} catch (Exception e) {
				e.printStackTrace(this.out);
				this.out.flush();
			}
		}

	}

	/**
	 * Closes the closable components and frees up resources.
	 */
	@Override
	public void close() {
		this.out.flush();
		try {
			this.out.close();
			this.raw_out.close();
			this.modem.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
