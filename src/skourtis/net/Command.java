package skourtis.net;

import skourtis.Code;
import skourtis.utils.PairView;

/**
 * A {@linkplain PairView} of {@linkplain Code} and arguments as
 * {@linkplain String} array.
 * 
 * @author Antonis Skourtis
 * @see {@link PairView}
 */
public class Command extends PairView<Code, String[]> {

	/**
	 * Default constructor.
	 */
	public Command(Code code, String... args) {
		super(code, args);
	}

	/**
	 * Returns the code of the command. Does the same as
	 * {@linkplain Command#getFirst()}.
	 * 
	 * @return the code
	 */
	public Code getCode() {
		return this.getFirst();
	}

	/**
	 * Returns the arguments of the command. Does the same as
	 * {@linkplain Command#getSecond()}.
	 * 
	 * @return the arguments
	 */
	public String[] getArgs() {
		return this.getSecond();
	}

	/**
	 * Constructs and returns the command as a byte array.
	 * 
	 * @return the command as a byte array
	 */
	public byte[] getBytes() {
		String tmp = this.getCode().getStringValue();
		for (String arg : this.getArgs())
			tmp += arg;
		return (tmp + '\r').getBytes();
	}

}
