package skourtis.net;

import java.util.*;

/**
 * A dynamic buffer of bytes.
 * 
 * @author Antonis Skourtis
 */
public class DynamicByteBuffer {

	private final ArrayList<Byte> list;

	/**
	 * Constructs the buffer with the default values.
	 */
	public DynamicByteBuffer() {
		this.list = new ArrayList<Byte>();
	}

	/**
	 * Constructs the buffer with the given initial capacity.
	 * 
	 * @param capacity - the initial capacity
	 */
	public DynamicByteBuffer(int capacity) {
		this.list = new ArrayList<Byte>(capacity);
	}

	/**
	 * Constructs the buffer with the given initial values.
	 * 
	 * @param buffer - the initial values
	 */
	public DynamicByteBuffer(byte[] buffer) {
		this();
		this.appendAll(buffer);
	}

	/**
	 * Appends an element to the end of the buffer.
	 * 
	 * @param element - the element to add
	 */
	public void append(byte element) {
		this.list.add(element);
	}

	/**
	 * Appends all elements of the array to the buffer.
	 * 
	 * @param elements - the array to be appended
	 */
	public void appendAll(byte[] elements) {
		for (byte element : elements)
			this.list.add(element);
	}

	/**
	 * Converts the buffer to an array.
	 * 
	 * @return the elements in order as an array.
	 */
	public byte[] toArray() {
		byte[] ret = new byte[this.list.size()];
		for (int i = 0; i < ret.length; ++i)
			ret[i] = this.list.get(i);
		return ret;
	}

	/**
	 * Checks if the buffer starts with a given prefix.
	 * 
	 * @param prefix - the given prefix
	 * @return true if the buffer starts with the prefix false otherwise and if the
	 *         prefix is null.
	 */
	public boolean startsWith(byte[] prefix) {
		if (prefix == null || prefix.length > this.list.size())
			return false;

		for (int i = 0; i < prefix.length; ++i)
			if (prefix[i] != this.list.get(i).byteValue())
				return false;

		return true;
	}

	/**
	 * Checks if the buffer ends with a given suffix.
	 * 
	 * @param suffix - the given suffix
	 * @return true if the buffer ends with the suffix false otherwise and if the
	 *         suffix is null.
	 */
	public boolean endsWith(byte[] suffix) {
		if (suffix == null || suffix.length > this.list.size())
			return false;

		for (int i = suffix.length - 1; i >= 0; --i)
			if (suffix[i] != this.list.get(i + this.list.size() - suffix.length).byteValue())
				return false;

		return true;
	}

}
