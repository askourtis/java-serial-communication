package skourtis.net;

import skourtis.utils.PairView;

/**
 * A {@linkplain PairView} of {@linkplain DynamicByteBuffer} and a latency as
 * {@linkplain Long}
 * 
 * @author Antonis Skourtis
 * @see {@link PairView}
 */
public class Response extends PairView<byte[], Long> {

	/**
	 * Constructs the object with the given parameters.
	 * 
	 * @param buffer - the buffer
	 * @param millis - the latency
	 */
	public Response(byte[] buffer, long millis) {
		super(buffer, millis);
	}

	/**
	 * Returns the buffer as an array.
	 * 
	 * @return the array buffer
	 */
	public byte[] getBuffer() {
		return this.getFirst();
	}

	/**
	 * Returns the latency of the response. Does the same as
	 * {@linkplain Response#getSecond()}
	 * 
	 * @return the latency
	 */
	public long getLatency() {
		return this.getSecond();
	}

}
