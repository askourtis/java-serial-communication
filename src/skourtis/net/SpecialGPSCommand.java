package skourtis.net;

import skourtis.Code;
import skourtis.Main;

//TODO Exception handling in given values.?
/**
 * A Special GPS command. It acquires the needed data for a satellite image.
 * 
 * @author Antonis Skourtis
 *
 */
public class SpecialGPSCommand extends Command {

	private final int route;
	private int trace;
	private int number;
	private boolean active;
	private final int timeDiff;
	private float previousTime;

	/**
	 * Default constructor.
	 * 
	 * @param route    - the chosen route (1-9)
	 * @param trace    - the begin trace (0 - 9999)
	 * @param number   - the number of traces to pick (0 - 9)
	 * @param timeDiff - the minimum time difference in seconds of each of the
	 *                 selected traces
	 */
	public SpecialGPSCommand(int route, int trace, int number, int timeDiff) {
		super(Code.GPS, new String[number]);
		this.route = route;
		this.trace = trace;
		this.number = number;
		this.active = false;
		this.timeDiff = timeDiff;
		this.previousTime = Float.NaN;
	}

	/**
	 * Activates the command and returns the next needed commands for the
	 * acquisition of the needed data, or null if no more data is required.
	 * 
	 * @return the next command or null
	 */
	public Command getNextCommand() {
		if (!this.isActive())
			this.active = true;
		if (this.number == 0)
			return null;
		int numberOfTraces = (this.timeDiff * this.number) % 100;
		Command ret = new Command(Code.GPS, String.format("R=%01d%04d%02d", this.route, this.trace, numberOfTraces));
		this.trace += numberOfTraces;
		return ret;
	}

	/**
	 * Returns if the command is active, meaning if it has send its first needed
	 * commands.
	 * 
	 * @return true if active, false otherwise
	 */
	public boolean isActive() {
		return this.active;
	}

	/**
	 * Processes the given buffer. Only lines of $GPGGA will be processed.
	 * 
	 * @param buffer - the byte buffer
	 */
	public void processData(byte[] buffer) {
		String[] args = this.getArgs();
		String response = new String(buffer, Main.GPS_PREFIX.length,
				buffer.length - Main.GPS_SUFFIX.length - Main.GPS_PREFIX.length);
		String[] lines = response.split("\r\n");

		for (String line : lines) {
			String[] split = line.split(",");
			if (split.length != 0 && split[0].equals("$GPGGA")) {
				float now = Float.parseFloat(split[1]);
				if (Float.isNaN(this.previousTime) || now - this.previousTime >= this.timeDiff) {
					this.previousTime = now;
					String latitude = split[2];
					String longitude = split[4];

					int latDeg = Integer.parseUnsignedInt(latitude.substring(0, 2));
					float latMinf = Float.parseFloat(latitude.substring(2));
					int latMin = (int) latMinf;
					int latSec = (int) ((latMinf - latMin) * 60);

					int longDeg = Integer.parseUnsignedInt(longitude.substring(0, 3));
					float longMinf = Float.parseFloat(longitude.substring(3));
					int longMin = (int) longMinf;
					int longSec = (int) ((longMinf - longMin) * 60);

					args[args.length - this.number] = String.format("T=%02d%02d%02d%02d%02d%02d", longDeg, longMin,
							longSec, latDeg, latMin, latSec);

					if (--this.number == 0)
						return;
				}
			}
		}

	}

}
