package skourtis.net;

import skourtis.Code;

/**
 * A special command. The main characteristic of this command is that it should
 * be sent for a time interval.
 * 
 * @author Antonis Skourtis
 *
 */
public class TimedCommand extends Command {

	private final long time;

	/**
	 * Default constructor.
	 */
	public TimedCommand(Code code, long time, String... args) {
		super(code, args);
		this.time = time;
	}

	/**
	 * Returns the time/duration of the command.
	 * 
	 * @return the time
	 */
	public long getTime() {
		return this.time;
	}

}
