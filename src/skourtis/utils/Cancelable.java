package skourtis.utils;

/**
 * An implementation of {@linkplain Runnable} with the added feature 'process
 * cancel'. The Object can be in one of two states, either cancelled or not. The
 * Implementation of the {@linkplain Runnable#run()} method should take the
 * state into consideration. Long processes such as loops or recursive methods
 * should have a check of state built into them. Note that only the
 * implementation can guarantee that the process will be canceled.
 * 
 * 
 * @author Antonis Skourtis
 * 
 * @see {@link Runnable}, {@link Runnable#run()}
 *
 */
public abstract class Cancelable implements Runnable {

	private boolean cancel = false;

	/**
	 * Sets the state to the given boolean.
	 * 
	 * @param cancel - the new state
	 */
	public void setCancel(boolean mayCancel) {
		this.cancel = mayCancel;
	}

	/**
	 * Returns the state.
	 * 
	 * @return the state
	 */
	public boolean isCancelled() {
		return this.cancel;
	}

}
