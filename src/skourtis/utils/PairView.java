package skourtis.utils;

/**
 * A structure of two final objects.
 * 
 * @author Antonis Skourtis
 *
 * @param <T1> - The type of the first object.
 * @param <T2> - The type of the second object.
 */
public class PairView<T1, T2> {

	private final T1 first;
	private final T2 second;

	/**
	 * Default constructor.
	 * 
	 * @param first  - the first value
	 * @param second - the second value
	 */
	public PairView(T1 first, T2 second) {
		this.first = first;
		this.second = second;
	}

	/**
	 * Returns the first element.
	 * 
	 * @return the first element
	 */
	public T1 getFirst() {
		return first;
	}

	/**
	 * Returns the second element.
	 * 
	 * @return the second element
	 */
	public T2 getSecond() {
		return second;
	}

	/**
	 * Checks if this pair is equal to an other object.
	 */
	@Override
	public boolean equals(Object other) {

		if (super.equals(other))
			return true;

		if (other instanceof PairView<?, ?>) {
			PairView<?, ?> cast = (PairView<?, ?>) other;
			if (cast.getFirst().equals(this.getFirst()) && cast.getSecond().equals(this.getSecond()))
				return true;
		}

		return false;
	}

	/**
	 * Clones the pair.
	 */
	@Override
	public PairView<T1, T2> clone() {
		return new PairView<>(this.getFirst(), this.getSecond());
	}

	/**
	 * Returns a string representation of the pair.
	 */
	@Override
	public String toString() {
		return String.format("<%s, %s>", this.getFirst().toString(), this.getSecond().toString());
	}

}
