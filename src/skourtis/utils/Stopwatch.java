package skourtis.utils;

/**
 * A helper class, made to time actions.
 * 
 * @author Antonis Skourtis
 */
public class Stopwatch {

	private long begin = 0L, end = 0L;
	private boolean running = false;

	/**
	 * Starts/Restarts the stopwatch.
	 */
	public void start() {
		if (!this.isRunning()) {
			this.running = true;
			this.begin = System.currentTimeMillis();
		}
	}

	/**
	 * Stops the stopwatch.
	 */
	public void stop() {
		if (this.isRunning()) {
			this.end = System.currentTimeMillis();
			this.running = false;
		}
	}

	/**
	 * Returns the duration between that start and the stop of the stopwatch.
	 * 
	 * @return the duration, if the stop watch is not stopped returns the duration
	 *         of the start and now.
	 */
	public long getDuration() {
		if (this.isRunning())
			return System.currentTimeMillis() - this.begin;
		return this.end - this.begin;
	}

	/**
	 * Returns if the stopwatch is running.
	 * 
	 * @return if it's running
	 */
	public boolean isRunning() {
		return this.running;
	}

}
